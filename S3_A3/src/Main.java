import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int num = 0;
        Scanner in = new Scanner(System.in);

        System.out.println("Input an integer whose factorial will be computed:");
        try{
            num = in.nextInt();
            if(num <= 0){
                throw new Exception("Invalid input.");
            }
        } catch(Exception e){
            System.out.println(e + " Please enter an integer greater than 0.");
            e.printStackTrace();
            return;
        }
        int answer = 1;
        int counter = 1;
        while(counter<=num){
            answer = answer * counter;
            counter++;
        }
        System.out.println("The factorial of " + num + " is " + answer);

        for(int i = counter; i <= num; i++){
            answer = answer * i;
        }
        System.out.println("The factorial of " + num + " is " + answer + " (for loop used)");


        for(int i = 0; i < 5; i++){
            System.out.print("\n");
            for(int j = 0; j <= i; j++){
                System.out.print("* ");
            }
        }
    }
}