package com.zuitt.example;

import java.util.Scanner;

public class Exceptions {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //The Scanner object looks for which input to scan. In this case, it ts the console(System.in)
        //The Scanner object has multiple methods that can be used to get the expected value
        /*
        * nextInt() - expects an integer
        * nextDouble() - expects a double
        * nextLine() - get the entire line as a String
        *
        */
        System.out.println("Input a number: ");

        int num = 0;

        try { //Try to do this block of code
            num = input.nextInt(); //The nextInt() method tells Java that it is expecting an integer value as input.
        } catch(Exception e){ //catch any errors
            System.out.println("Invalid input.");
            e.printStackTrace();
            return;
        }

        System.out.println("You have entered: " + num);
    }
}
