package com.zuitt;

public class Contact {
    private String name;
    private String contactNumber;
    private String address;

    public Contact(){
    }

    public Contact(String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    public String getName(){
        return name;
    }
    public void setName(String newName){
        this.name = newName;
    }

    public String getContactNumber(){
        return contactNumber;
    }
    public void setContactNumber(String newNumber){
        this.contactNumber = newNumber;
    }

    public String getAddress(){
        return address;
    }
    public void setAddress(String newAddress){
        this.address = newAddress;
    }
}
