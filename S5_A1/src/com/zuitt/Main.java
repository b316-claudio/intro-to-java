package com.zuitt;

import javax.swing.plaf.IconUIResource;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe", "+63152468596", "Quezon City");
        Contact contact2 = new Contact("Jane Doe", "+63162148573", "Caloocan City");

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);


        if(phonebook.getContacts().isEmpty()){
            System.out.println("Phonebook List is Empty.");
        } else{
            System.out.println("Phonebook Contact List:");
            for(Contact contact : phonebook.getContacts()){
                System.out.println("\nName: " + contact.getName());

                System.out.println("-------------------------------------");

                System.out.println(contact.getName() + " has the following registered numbers:\n" + contact.getContactNumber());

                System.out.println("-------------------------------------");

                System.out.println(contact.getName() + " has the following registered addresses:\n" + contact.getAddress());

                System.out.println("\n=========================================\n");
            }
        }
    }
}
