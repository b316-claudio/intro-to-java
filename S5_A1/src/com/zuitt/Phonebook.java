package com.zuitt;

import java.util.ArrayList;

public class Phonebook {
    private Contact contact;
    private ArrayList<Contact> contacts = new ArrayList<>();
    public Phonebook(){
    }

    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    public void addContact(Contact contact){
        this.contact = contact;
        this.contacts.add(contact);
    }

    public String getContactName(){
        return contact.getName();
    }
    public String getContactNum(){
        return contact.getContactNumber();
    }
    public String getAddress(){
        return contact.getAddress();
    }

    public ArrayList<Contact> getContacts(){
        return contacts;
    }
}
