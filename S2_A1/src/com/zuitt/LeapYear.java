package com.zuitt;

import java.util.Scanner;

public class LeapYear {
    public static void main(String[] args) {
        int year;
        Scanner yearInput = new Scanner(System.in);

        System.out.println("Input year to be checked if a leap year.");
        year = Integer.parseInt(yearInput.nextLine());

        if((year%4 == 0 && year%100 != 0) || year%400 == 0){
            System.out.println(year + " is a leap year");
        } else {
            System.out.println(year + " is NOT a leap year");
        }
    }
}
