package com.zuitt.example;

public class Person implements Actions {
    //For a class to implement or use an interface, we use the implements keyword
    //Can implement multiple interface
    public void sleep(){
        System.out.println("Zzzzzzzzzz.......");
    }

    public void run(){
        System.out.println("Running on the road");
    }

    /*
    * Mini Activity
    * Create two new actions that a person would do.
    * and Implement them in the actions class.
    * run them in the Main class
    */

    public void eat(){
        System.out.println("Itadakimsu!");
    }

    public void commute(){
        System.out.println("Bayad po.");
    }
}
