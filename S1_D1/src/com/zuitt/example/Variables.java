package com.zuitt.example;

public class Variables {
    public static void main(String[] args) {
        //Variable Declaration
        int age;
        char middle_name;

        //Variable Declaration vs initialization
        int x;
        int y = 0;

        //Print out into the console "The value of y is "
        System.out.println("The value of y is " + y);

        //Reinitialization
        y = 10;
        System.out.println("The value of y is " + y);
    }
}
