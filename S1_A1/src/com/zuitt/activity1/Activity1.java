package com.zuitt.activity1;
import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;

        Scanner firstname = new Scanner(System.in);
        Scanner lastname = new Scanner(System.in);
        Scanner subject1 = new Scanner(System.in);
        Scanner subject2 = new Scanner(System.in);
        Scanner subject3 = new Scanner(System.in);


        System.out.println("First Name:");
        firstName = firstname.nextLine();
        System.out.println("Last Name:");
        lastName = lastname.nextLine();
        System.out.println("First Subject Grade:");
        firstSubject = Double.parseDouble(subject1.nextLine());
        System.out.println("Second Subject Grade:");
        secondSubject = Double.parseDouble(subject2.nextLine());
        System.out.println("Third Subject Grade:");
        thirdSubject = Double.parseDouble(subject3.nextLine());


        System.out.println("Good day, " + firstName + lastName);
        double average = (firstSubject + secondSubject + thirdSubject)/3;
        System.out.println("Your grade average is: " + average);
    }
}
