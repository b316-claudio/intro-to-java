package com.zuitt;

public class PrimeNumber {
    public static void main(String[] args) {
        int[] prime = {2, 3, 5, 7, 11};

        String primeSearch = "first";

        switch (primeSearch){
            case "first":
                System.out.println("The first prime number is: " + prime[0]);
                break;

            case "second":
                System.out.println("The second prime number is: " + prime[1]);
                break;

            case "third":
                System.out.println("The third prime number is: " + prime[2]);
                break;

            case "fourth":
                System.out.println("The fourth prime number is: " + prime[3]);
                break;

            case "fifth":
                System.out.println("The fifth prime number is: " + prime[4]);
                break;

            default:
                System.out.println("Invalid");
        }
    }
}
