package com.zuitt;

import java.util.HashMap;

public class Hashmap {
    public static void main(String[] args) {
        HashMap<String, Integer>object = new HashMap<String, Integer>();

        object.put("toothpaste", 15);
        object.put("toothbrush", 20);
        object.put("soap", 12);

        System.out.println("Our current inventory consists of: " + object);
    }
}
