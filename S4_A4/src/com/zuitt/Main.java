package com.zuitt;

public class Main {
    public static void main(String[] args) {
        User user1 = new User("Ralph Claudio", 26, "ralph@mail.com", "Plaridel, Bulacan");


        Course course1 = new Course();
        course1.setCourseName("Main Course Package");
        course1.setDescription("This course consists of HTML, CSS, Bootstrap, JavaScript, MongoDB, NodeJS, ReactJS");
        course1.setSeats(30);
        course1.setFee(50000);
        course1.setStartDate();

        course1.setInstructor(user1);


        user1.greet();
        course1.welcomeMessage();
    }
}
