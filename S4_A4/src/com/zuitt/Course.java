package com.zuitt;

import java.util.Date;

public class Course {
    private String name;
    private String description;
    private int seats;
    private double fee;
    private Date startDate;
    private Date endDate;
    private User instructor;

    public Course(){
    }

    public Course(String name, String description, int seats, double fee){
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = new Date();
//        this.instructor = new User();
    }

    public String getCourseName(){
        return name;
    }
    public void setCourseName(String courseName){
        this.name = courseName;
    }

    public String getDescription(){
        return description;
    }
    public void setDescription(String description){
        this.description = description;
    }

    public int getSeats(){
        return seats;
    }
    public void setSeats(int seats){
        this.seats = seats;
    }

    public double getFee(){
        return fee;
    }
    public void setFee(double newFee){
        this.fee = newFee;
    }

    public Date getStartDate(){
        return startDate;
    }

    public void setStartDate(){
        this.startDate = new Date();
    }

    public void setEndDate(){
        this.endDate = new Date();
    }
    public Date getEndDate(){
        return endDate;
    }

    public User getInstructor(){
        return instructor;
    }

    public void setInstructor(User instructor){
        this.instructor = instructor;
    }

    public void welcomeMessage(){
        System.out.println("\nWelcome to the course " + this.getCourseName() + ". Course description: " + this.getDescription() + ". Your instructor for this course is " + this.getInstructor().getName() + ". Start Date: " + this.getStartDate() + "\nSeats: " + this.getSeats() + "\nFee: P" + this.getFee());
    }
}
