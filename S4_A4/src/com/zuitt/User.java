package com.zuitt;

public class User {
    private String name;
    private int age;
    private String email;
    private String address;

    public User(){
    }
    public User(String name, int age, String email, String address){
        this.name = name;
        this.age = age;
        this.email =  email;
        this.address = address;
    }

    public String getName(){
        return name;
    }
    public void setName(String newName){
        this.name = newName;
    }

    public int getAge(){
        return age;
    }
    public void setAge(int newAge){
        this.age = newAge;
    }

    public String getEmail(){
        return email;
    }
    public void setEmail(String newEmail){
        this.email = newEmail;
    }

    public String getAddress(){
        return address;
    }
    public void setAddress(String newAddress){
        this.address = newAddress;
    }

    public void greet(){
        System.out.println("\nHello, my name is " + this.getName() + ". I am " + this.getAge() + " years old. For questions and inquiry you may send it to my email: " + this.getEmail() + ". I am from " + this.getAddress() + ". Nice to meet you.");
    }
}
