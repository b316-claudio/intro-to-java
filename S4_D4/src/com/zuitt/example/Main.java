package com.zuitt.example;

import javax.sound.midi.Soundbank;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car();
        car1.setMake("Veyron");
        car1.setBrand("Bugatti");
        car1.setPrice(200000);
        System.out.println(car1.getBrand());
        System.out.println(car1.getMake());
        System.out.println(car1.getPrice());

        //Instance - an object created from a class and each instance should be independent of each other

        /*
        Mini-Activity:

        Create two new instances of the Car class and save it in a variable called car2 and car3 respectively.
        Access the properties of the instance and update its values.
            setMake = String
            brand = String
            price = int
         You can come up with your own values.
         Print the values of each property of the instance.
        */

        Car car2 = new Car();
        car2.setMake("Corvette");
        car2.setBrand("Chevrolet");
        car2.setPrice(12000000);

        Car car3 = new Car();
        car3.setMake("Civic");
        car3.setBrand("Honda");
        car3.setPrice(1600000);

        System.out.println(car2.getBrand());
        System.out.println(car2.getMake());
        System.out.println(car2.getPrice());

        System.out.println(car3.getBrand());
        System.out.println(car3.getMake());
        System.out.println(car3.getPrice());

        Driver driver1 = new Driver("Alejandro", 25);

        System.out.println(driver1.getName());
        System.out.println(driver1.getAge());

        car1.start();
        car2.start();
        car3.start();

        System.out.println(car1.getCarDriver().getName());

        Driver newDriver = new Driver("Antonio", 21);
        car1.setCarDriver(newDriver);

        //Get name of new carDriver
        System.out.println(car1.getCarDriver().getName());
        System.out.println(car1.getCarDriver().getAge());

        System.out.println(car1.getCarDriverName());

        //Mini-Acitivity
        /* Create a new class called Animal with the following attributes:
        name - string
        color - string

        Add constructors, getters and setters for the class.
        */

        Animal animal1 = new Animal("Yuna", "Black");
        System.out.println("Animal: " + animal1.getName());
        System.out.println("Color: " + animal1.getColor());

        Animal animal2 = new Animal("Clifford", "Red");
        animal2.call();

        Dog dog1 = new Dog();
        System.out.println(dog1.getName());
        dog1.call();

        dog1.setName("Hachiko");
        System.out.println(dog1.getName());
        dog1.greet();
        dog1.setColor("Brown");
        System.out.println(dog1.getColor());

        Dog dog2 = new Dog("Mike", "Dark Brown", "Corgi");
        dog2.call();
        System.out.println(dog2.getName());
        System.out.println(dog2.getDogBreed());
        dog2.greet();
    }
}
